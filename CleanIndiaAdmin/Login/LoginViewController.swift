//
//  LoginViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func login() {
        let email = emailTextField.text
        let pwd = pwdTextField.text
        
        if email == "" || pwd == "" {
            self.displayAlert(userMessage: "All Fields Necessary")
            return
        } else if email == "guneetgarg.2011@gmail.com" && pwd == "guneetgarg" {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "InitialNavigatorController")
            self.present(viewController!, animated: true, completion: nil)
        } else {
            self.displayAlert(userMessage: "Please check your credentials!")
        }
        
    }
}
