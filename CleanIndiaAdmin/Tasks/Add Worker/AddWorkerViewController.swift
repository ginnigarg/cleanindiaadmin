//
//  AddWorkerViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import AWSMobileClient

class AddWorkerViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var cnfPwdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func addWorker() {
        if nameTextField.text == "" || phoneTextField.text == "" || emailTextField.text == "" || pwdTextField.text == "" || cnfPwdTextField.text == "" {
            self.displayAlert(userMessage: "All fields Compulsory!")
            return
        }
        if pwdTextField.text == cnfPwdTextField.text {
            AWSMobileClient.sharedInstance().signUp(username: emailTextField.text!, password: pwdTextField.text!, userAttributes: ["email": emailTextField.text!, "phone_number": "+91\(phoneTextField.text!)", "name": nameTextField.text!]) { (signUpResult, error) in
                print(error.debugDescription)
                    if let signUpResult = signUpResult {
                        
                        switch(signUpResult.signUpConfirmationState) {
                            case .confirmed:
                                DispatchQueue.main.async {
                                    self.displayAlert(userMessage: "Worker has been added")
                                }
                                print("User is signed up and confirmed.")
                            case .unconfirmed:
                                
                                DispatchQueue.main.async {
                                    self.displayAlert(userMessage: "User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                                }
                            case .unknown:
                                print("Unexpected case")
                        }
                        
                    } else if let error = error {
                        if let error = error as? AWSMobileClientError {
                            switch(error) {
                            case .usernameExists(let message):
                                DispatchQueue.main.async {
                                    self.displayAlert(userMessage: message)
                                }
                                print(message)
                            default:
                                break
                            }
                        }
                        print("\(error.localizedDescription)")
                    }
            }
            
        } else {
            pwdTextField.text = ""
            cnfPwdTextField.text = ""
            self.displayAlert(userMessage: "Passwords Do Not Match!")
            return
        }
    }
}
