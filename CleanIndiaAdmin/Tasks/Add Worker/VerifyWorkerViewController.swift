//
//  VerifyWorkerViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 28/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import AWSMobileClient

class VerifyWorkerViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func verifyUser() {
        let email = emailTextField.text
        let code = codeTextField.text
        
        AWSMobileClient.sharedInstance().confirmSignUp(username: email!, confirmationCode: code!) { (signUpResult, error) in
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    DispatchQueue.main.async {
                        self.displayAlert(userMessage: "Worker Verified")
                    }
                    print("User is signed up and confirmed.")
                case .unconfirmed:
                    DispatchQueue.main.async {
                        self.displayAlert(userMessage: "Incorrect Password")
                    }
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                case .unknown:
                    print("Unexpected case")
                }
            } else if let error = error {
                print("\(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func resendCode() {
        AWSMobileClient.sharedInstance().resendSignUpCode(username: emailTextField.text!, completionHandler: { (result, error) in
            if let signUpResult = result {
                DispatchQueue.main.async {
                    self.displayAlert(userMessage: "A verification code has been sent via \(signUpResult.codeDeliveryDetails!.deliveryMedium) at \(signUpResult.codeDeliveryDetails!.destination!)")
                }
                print("A verification code has been sent via \(signUpResult.codeDeliveryDetails!.deliveryMedium) at \(signUpResult.codeDeliveryDetails!.destination!)")
            } else if let error = error {
                print("\(error.localizedDescription)")
            }
        })
    }
}
