//
//  CheckStatusViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class CheckStatusViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addAnnotations()
    }
    
    func addGesture() {
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotation(_ : )))
        gesture.minimumPressDuration = 1
        mapView.addGestureRecognizer(gesture)
    }
    
    @objc func addAnnotation(_ sender: UILongPressGestureRecognizer) {
        
    }
    
    func addAnnotations() {
        
    }
    
    func mapView (_ mapView : MKMapView , didSelect view : MKAnnotationView) {
        
    }
}
