//
//  AddDustbinWorkerDetailViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit

class AddDustbinWorkerDetailViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func continuePressed() {
        if email.text == "" || phone.text == "" {
            self.displayAlert(userMessage: "Please fill in all the details!")
            return
        }
        DustbinConstants.constants.email = email.text
        DustbinConstants.constants.phone = phone.text
        self.performSegue(withIdentifier: "AddDustbinDetail", sender: self)
    }
    
}
