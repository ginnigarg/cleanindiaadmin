//
//  DustbinConstants.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit

class DustbinConstants {
    
    static var constants = DustbinConstants()
    
    var email: String?
    var phone: String?
    var lat: String?
    var long: String?
}
