//
//  AddDustbinDustbinDetailViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class AddDustbinDustbinDetailViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var coords = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
        mapView.delegate = self
        coords.latitude = 0.0
        coords.longitude = 0.0
    }
    
    func addGesture() {
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotation(_ : )))
        gesture.minimumPressDuration = 1
        mapView.addGestureRecognizer(gesture)
    }
    
    @objc func addAnnotation(_ sender: UILongPressGestureRecognizer) {
        let location = sender.location(in: mapView)
        coords = mapView.convert(location, toCoordinateFrom: mapView)
        let mapAnnotation = MKPointAnnotation()
        mapAnnotation.coordinate = coords
        mapView.addAnnotation(mapAnnotation)
    }
    
    @IBAction func addDustbin() {
        DustbinConstants.constants.lat = (String)(coords.latitude)
        DustbinConstants.constants.long = (String)(coords.longitude)
        if DustbinConstants.constants.lat == "0.0" || DustbinConstants.constants.long == "0.0" {
            self.displayAlert(userMessage: "Please Select the dustbin location!")
            return
        }
        self.displayAlert(userMessage: "Dustbin successfully added")
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
}

