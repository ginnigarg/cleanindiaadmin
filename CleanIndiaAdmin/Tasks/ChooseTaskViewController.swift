//
//  ChooseTaskViewController.swift
//  CleanIndiaAdmin
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit

class ChooseTaskViewController: UIViewController {
    
    @IBAction func addWoker() {
        self.performSegue(withIdentifier: "AddWorker", sender: self)
    }
    
    @IBAction func addDustbin() {
        self.performSegue(withIdentifier: "AddDustbinWorker", sender: self)
    }
    
    @IBAction func checkStatus() {
        self.performSegue(withIdentifier: "CheckStatus", sender: self)
    }
}
